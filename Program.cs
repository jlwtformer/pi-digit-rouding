﻿using System;

namespace Pi_Digits
{
	class Program
	{
		static void Main(string[] args)
		{
			const double pi = Math.PI;
			int pi_nums;

			do
			{
				Console.WriteLine("Enter the number of digits of pi to show (no more than 30):");
				try
				{
					pi_nums = Convert.ToInt32(Console.ReadLine());
				}
				catch (FormatException e)
				{
					Console.WriteLine("That's not a number.");
					pi_nums = 100;
				}
			} while (IsProperValue(pi_nums) == false);

			Console.WriteLine(Math.Round(pi, pi_nums));
		}

		public static bool IsProperValue(int value)
		{
			if (value >= 31)
			{
				Console.WriteLine("Please enter a valid number.");
				return false;
			}

			return true;
		}
	}
}
